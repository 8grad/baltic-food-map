# Baltic Food Map

License: GPLv2 or later

Description:
**places an interactive Mundraub.org map as iframe into your post/ page**


Installation:

1. Upload the plugin as folder named *balticFoodMap* to your blog, activate it;
2. place the following **[/] shortcode** into your post/ page:


`[baltic-food-map cta="Neue Entdeckungen eintragen!" headline="Interaktive Karte zu erntbaren Pflanzen in und um Lauenburg"]`


1, 2, 3: You're done!

![Screenshot](screenshot.png)

Changelog:
