<?php
/**
 * Plugin Name:       Baltic Food Map
 * Plugin URI:        https://marcwright.de/
 * Description:       Interactive food map of eatable plats around Lauenburg/ Germany on base of Mundraub.org
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Marc Wright
 * Author URI:        https://marcwright.de/
 * License:           GPLv2 or later
 * License URI:       http://www.apache.org/licenses/LICENSE-2.0.html
 * Text Domain:       baltic-food-map-plugin
 */


// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
// We need some CSS.
function balticRAW_css() {
    echo "
    <style type='text/css'>
    body {
        background-color: white;
    }
    
    .balticFoodMap {
        font-family: 'Source Sans Pro', sans-serif;
    }
    
    .balticFoodMap .balticFoodMap__headline {
        margin-top: 0;
        font-weight: 900;
        margin-bottom: 1rem;
        text-transform: uppercase;
        font-size: 1.5rem;
        margin-left: 1rem;
    }
    
    .balticFoodMap .balticFoodMap__cta {
        margin-left: 1rem;
        margin-top: 1rem;
        padding: 0.5rem 1rem 0.5rem 1rem;
        display: inline-block;
        background-color: black;
        color: white;
        text-decoration: none;
    }
    
    .balticFoodMap #balticFoodMap__iframe {
        height: 400px;
        width:calc(100% - 1rem);
        margin-left: 0.5rem;
        margin-right: 0.5rem;
        margin-bottom: 0;
        filter: grayscale(100) contrast(90%) invert(100%);
        border: 3px solid white;
    }
    </style>
";
}

add_action('wp_head', 'balticRAW_css');

function balticRAW_displayMap($atts) {
    $headline = NULL;
    $cta = NULL;
    $addFoodToMap = "https://mundraub.org/map#z=15&lat=53.3789803&lng=10.5513889";

    extract(shortcode_atts(array(
        'headline' => 1,
        'cta' => 1,
    ), $atts));

    $map  = '<map class="balticFoodMap">';
    $map .= '<h3 class="balticFoodMap__headline">' . $headline . '</h3>';
    $map .= '<iframe id="balticFoodMap__iframe" scrolling="no" frameborder="0" src="http://leafletjs.com/examples/mobile/example.html"></iframe>';
    $map .= '<a target="_blank" href="' . $addFoodToMap . '" class="balticFoodMap__cta">' . $cta . '</a>';
    $map .= '</map>';
    return $map;
}

function balticRAW_register_shortcodes() {
    add_shortcode('baltic-food-map', 'balticRAW_displayMap');
}

add_action('init', 'balticRAW_register_shortcodes');
